SUMMARY="OpenGL Utility Library"
DESCRIPTION="OpenGL Utility Library"
HOMEPAGE="http://freedesktop.org"
COPYRIGHT="Copyright (C) 1999-2012 Brian Paul and others.  All Rights Reserved."
LICENSE="MIT"
SRC_URI="ftp://freedesktop.org/pub/mesa/glu/glu-9.0.0.tar.gz"
CHECKSUM_MD5="bbc57d4fe3bd3fb095bdbef6fcb977c4"
REVISION="2"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	glu$secondaryArchSuffix = $portVersion
	lib:libGLU$secondaryArchSuffix = 1.3.1 compat >= 1
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	"
BUILD_REQUIRES="
	devel:libglapi$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	cmd:sed
	"

BUILD()
{
	# The Haiku build system builds libGL.so from libglapi and libglu. Since the
	# glu build system requires libGL.so (since the shared libglu.so is linked
	# against it), we build a dummy libGL.so from libglapi.a.
	gcc -o libGL.so -shared -Xlinker --whole-archive -lglapi
	libglLinkFlags="-L$(pwd)"

	LDFLAGS="$libglLinkFlags" \
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libGLU
	fixPkgconfig

	# Remove the additional LDFLAGS from libGLU.la
	sed -i "s,^dependency_libs=.*,dependency_libs=' -lGL'," \
		$developLibDir/libGLU.la

	# devel package
	packageEntries devel \
		$developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	glu${secondaryArchSuffix}_devel = $portVersion
	devel:libGLU$secondaryArchSuffix = 1.3.1 compat >= 1
	"
REQUIRES_devel="
	glu$secondaryArchSuffix == $portVersion base
	"
